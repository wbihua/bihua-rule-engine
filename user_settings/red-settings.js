const redFunction = require('../function_global_context')
const userDir = require('path').join(process.env.rootPath, 'nodered')
const path = require('path')

const { JSDOM } = require( "jsdom" );
const { window } = new JSDOM( "" );
const $ = require( "jquery" )( window );

// Create the settings object - see default settings.js file for other options
module.exports = {
    httpAdminRoot: "/red",
    httpNodeRoot: "/api",
    userDir: userDir,
    ui: { path: '/ui' },
    functionGlobalContext: redFunction,    // enables global context
    flowFile:path.join(userDir,"flows_main.json"),
    adminAuth: {
        tokensOptions: {
            name: "bihua",
            title:"规则引擎认证不通过！",
            subTitle:"请通过碧华后台管理系统-规则编排访问规则引擎！",
            label: '碧华后台管理系统',
            icon:"fa-arrow-right",
            url:"http://bihua.vip.cpolar.top/login"
        },
        tokens: function(token) {
            return new Promise(function(resolve, reject) {
                // Do whatever work is needed to check token is valid
                $.ajax({
                    headers: {
                        "authorization":"Bearer "+token
                    },
                    cache: false,
                    url: "http://bihua.vip.cpolar.top/prod-api/iot/rulearrangement/tokens",
                    type: "GET",
                    success: function(data) {
                        console.info("tokens-api:"+ data.code);
                        if (data.code == 200) {
                            console.info("tokens-api-success:"+ token);
                            // Resolve with the user object. It must contain
                            // properties 'username' and 'permissions'
                            var user = { username: 'admin', permissions: '*' };
                            resolve(user);
                        } else {
                            console.info("tokens-api-fail:"+ token);
                            // Resolve with null as this user does not exist
                            resolve(null);
                        }
                    }
                });
            });
        }
    },
    //httpNodeAuth: {user:"user",pass:"$2a$08$1YPhh6WQdiqJo6gXCWB8IOUl5U7ajIIYMkIGBSQSaUQZG3MKUu2Ze"},
    nodeAuth: {
        tokens: function(token) {
            console.info("node-token:"+token);
            return new Promise(function(resolve, reject) {
                // Do whatever work is needed to check token is valid
                if (true) {
                    // Resolve with the user object. It must contain
                    // properties 'username' and 'permissions'
                    var user = { username: 'admin', permissions: '*' };
                    resolve(user);
                } else {
                    // Resolve with null as this user does not exist
                    resolve(null);
                }
            });
        }
    },
    httpNodeCors: {
        origin: "*",
        methods: "GET,PUT,POST,DELETE"
    },
    editorTheme: {
        page: {
            title: 'bihua-rule-engine',
            css: path.join(process.env.rootPath,'public','css','bihua.css'),
            scripts:path.join(process.env.rootPath,'public','js','bihua.js')
        },
        header: {
            title: "碧华规则引擎编辑器",
            image: path.join(process.env.rootPath,'public','images','logo.jpg'),
            url:'http://www.wbihua.com'
        },
        deployButton: {
            type:"simple",
            label:"Save",
            deployNotice: function(token, flowId) {
                //console.info("deployNotice-token:"+token);
                //console.info("deployNotice-flowId:"+flowId);
                $.ajax({
                    headers: {
                        "authorization":"Bearer "+token
                    },
                    cache: false,
                    url: "http://bihua.vip.cpolar.top/prod-api/iot/rulearrangement/updateRuleContent/"+flowId,
                    type: "GET",
                    success: function(data) {
                        console.info("deployNotice-success-data:"+ JSON.stringify(data));
                    }
                });
            },
            icon: path.join(process.env.rootPath,'public','images','save.png') // or null to remove image
        },
        menu: { // Hide unwanted menu items by id. see packages/node_modules/@node-red/editor-client/src/js/red.js:loadEditor for complete list
            "menu-item-import-library": false,
            "menu-item-export-library": false,
            "menu-item-keyboard-shortcuts": false,
            "menu-item-help": {
                label: "Alternative Help Link Text",
                url: "http://example.com"
            }
        },
    }
}

