const commonSettings = require('./common-settings')
const redSettings = require('./red-settings')

module.exports = {
    redSettings,
    commonSettings
}